﻿namespace orangesGame.Framework.GamePrefs
{
    /// <summary>
    /// 数据存储管理接口
    /// </summary>
    public interface IGamePrefsManager
    {
        /// <summary>
        /// 设置gameprefs的调用者
        /// </summary>
        /// <param name="gamePrefs"></param>
        void SetGamePrefs(IGamePrefs gamePrefs);

        /// <summary>
        /// 加载配置。
        /// </summary>
        /// <returns>是否加载配置成功。</returns>
        bool Load();

        /// <summary>
        /// 保存配置。
        /// </summary>
        /// <returns>是否保存配置成功。</returns>
        bool Save();

        /// <summary>
        /// 检查是否存在指定配置项。
        /// </summary>
        /// <param name="name">要检查配置项的名称。</param>
        /// <returns>指定的配置项是否存在。</returns>
        bool HasPrefs(string name);

        /// <summary>
        /// 移除指定配置项。
        /// </summary>
        /// <param name="name">要移除配置项的名称。</param>
        void RemovePrefs(string name);

        /// <summary>
        /// 清空所有配置项。
        /// </summary>
        void RemoveAllPrefs();

        /// <summary>
        /// 从指定配置项中读取整数值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <returns>读取的整数值。</returns>
        int GetInt(string name);

        /// <summary>
        /// 从指定配置项中读取整数值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <param name="defaultValue">当指定的配置项不存在时，返回此默认值。</param>
        /// <returns>读取的整数值。</returns>
        int GetInt(string name, int defaultValue);

        /// <summary>
        /// 向指定配置项写入整数值。
        /// </summary>
        /// <param name="name">要写入配置项的名称。</param>
        /// <param name="value">要写入的整数值。</param>
        void SetInt(string name, int value);

        /// <summary>
        /// 从指定配置项中读取浮点数值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <returns>读取的浮点数值。</returns>
        float GetFloat(string name);

        /// <summary>
        /// 从指定配置项中读取浮点数值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <param name="defaultValue">当指定的配置项不存在时，返回此默认值。</param>
        /// <returns>读取的浮点数值。</returns>
        float GetFloat(string name, float defaultValue);

        /// <summary>
        /// 向指定配置项写入浮点数值。
        /// </summary>
        /// <param name="name">要写入配置项的名称。</param>
        /// <param name="value">要写入的浮点数值。</param>
        void SetFloat(string name, float value);

        /// <summary>
        /// 从指定配置项中读取字符串值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <returns>读取的字符串值。</returns>
        string GetString(string name);

        /// <summary>
        /// 从指定配置项中读取字符串值。
        /// </summary>
        /// <param name="name">要获取配置项的名称。</param>
        /// <param name="defaultValue">当指定的配置项不存在时，返回此默认值。</param>
        /// <returns>读取的字符串值。</returns>
        string GetString(string name, string defaultValue);

        /// <summary>
        /// 向指定配置项写入字符串值。
        /// </summary>
        /// <param name="name">要写入配置项的名称。</param>
        /// <param name="value">要写入的字符串值。</param>
        void SetString(string name, string value);

        /// <summary>
        /// 从指定配置项中读取对象。
        /// </summary>
        /// <typeparam name="T">要读取对象的类型。</typeparam>
        /// <param name="name">要获取配置项的名称。</param>
        /// <returns>读取的对象。</returns>
        T GetObject<T>(string name);

        /// <summary>
        /// 从指定配置项中读取对象。
        /// </summary>
        /// <typeparam name="T">要读取对象的类型。</typeparam>
        /// <param name="name">要获取配置项的名称。</param>
        /// <param name="defaultObj">当指定的配置项不存在时，返回此默认对象。</param>
        /// <returns>读取的对象。</returns>
        T GetObject<T>(string name, T defaultObj);

        /// <summary>
        /// 向指定配置项写入对象。
        /// </summary>
        /// <typeparam name="T">要写入对象的类型。</typeparam>
        /// <param name="name">要写入配置项的名称。</param>
        /// <param name="obj">要写入的对象。</param>
        void SetObject<T>(string name, T obj);

    }
}
