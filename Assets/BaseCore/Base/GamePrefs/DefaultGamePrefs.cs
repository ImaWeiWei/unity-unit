﻿using System;
using UnityEngine;

namespace orangesGame.Framework.GamePrefs
{
    /// <summary>
    /// 默认的数据存储
    /// </summary>
    public class DefaultGamePrefs : IGamePrefs
    {
        public float GetFloat(string name)
        {
            return PlayerPrefs.GetFloat(name);
        }

        public float GetFloat(string name, float defaultValue)
        {
            return PlayerPrefs.GetFloat(name, defaultValue);
        }

        public int GetInt(string name)
        {
            return PlayerPrefs.GetInt(name);
        }

        public int GetInt(string name, int defaultValue)
        {
            return PlayerPrefs.GetInt(name, defaultValue);
        }

        public T GetObject<T>(string name)
        {
            throw new NotImplementedException();
        }

        public T GetObject<T>(string name, T defaultObj)
        {
            throw new NotImplementedException();
        }

        public string GetString(string name)
        {
            return PlayerPrefs.GetString(name);
        }

        public string GetString(string name, string defaultValue)
        {
            return PlayerPrefs.GetString(name, defaultValue);
        }

        public bool HasPrefs(string name)
        {
            return PlayerPrefs.HasKey(name);
        }

        public bool Load()
        {
            return true;
        }

        public void RemoveAllPrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        public void RemovePrefs(string name)
        {
            PlayerPrefs.DeleteKey(name);
        }

        public bool Save()
        {
            PlayerPrefs.Save();
            return true;
        }

        public void SetFloat(string name, float value)
        {
            PlayerPrefs.SetFloat(name, value);
        }

        public void SetInt(string name, int value)
        {
            PlayerPrefs.SetInt(name, value);
        }

        public void SetObject(string name, object obj)
        {
            throw new NotImplementedException();
        }

        public void SetObject<T>(string name, T obj)
        {
            throw new NotImplementedException();
        }

        public void SetString(string name, string value)
        {
            PlayerPrefs.SetString(name, value);
        }
    }
}
