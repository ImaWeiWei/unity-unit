﻿namespace orangesGame.Framework.GamePrefs
{
    /// <summary>
    /// 游戏的存储数据
    /// </summary>
    public static partial class GamePrefs
    {
        /// <summary>
        /// 音乐开关状态
        /// </summary>
        public static int musicState = 0;

        /// <summary>
        /// 震动开关状态
        /// </summary>
        public static int shakeState = 0;

        /// <summary>
        /// 玩家状态
        /// </summary>
        public static int playerState = 0;
    }
}
