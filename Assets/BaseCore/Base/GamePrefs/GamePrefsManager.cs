﻿using System;


namespace orangesGame.Framework.GamePrefs
{
    public class GamePrefsManager : IGamePrefsManager
    {
        IGamePrefs _gamePrefs;

        public float GetFloat(string name)
        {
            return _gamePrefs.GetFloat(name);
        }

        public float GetFloat(string name, float defaultValue)
        {
            return _gamePrefs.GetFloat(name, defaultValue);
        }

        public int GetInt(string name)
        {
            return _gamePrefs.GetInt(name);
        }

        public int GetInt(string name, int defaultValue)
        {
            return _gamePrefs.GetInt(name, defaultValue);
        }

        public T GetObject<T>(string name)
        {
            throw new NotImplementedException();
        }

        public T GetObject<T>(string name, T defaultObj)
        {
            throw new NotImplementedException();
        }

        public string GetString(string name)
        {
            return _gamePrefs.GetString(name);
        }

        public string GetString(string name, string defaultValue)
        {
            return _gamePrefs.GetString(name, defaultValue);
        }

        public bool HasPrefs(string name)
        {
            return _gamePrefs.HasPrefs(name);
        }

        public bool Load()
        {
            return _gamePrefs.Load();
        }

        public void RemoveAllPrefs()
        {
            _gamePrefs.RemoveAllPrefs();
        }

        public void RemovePrefs(string name)
        {
            _gamePrefs.RemovePrefs(name);
        }

        public bool Save()
        {
            return _gamePrefs.Save();
        }

        public void SetFloat(string name, float value)
        {
            _gamePrefs.SetFloat(name,value);
        }

        public void SetGamePrefs(IGamePrefs gamePrefs)
        {
            _gamePrefs = gamePrefs;
        }

        public void SetInt(string name, int value)
        {
            _gamePrefs.SetInt(name, value);
        }

        public void SetObject<T>(string name, T obj)
        {
            _gamePrefs.SetObject(name, obj);
        }

        public void SetString(string name, string value)
        {
            _gamePrefs.SetString(name, value);
        }
    }
}
