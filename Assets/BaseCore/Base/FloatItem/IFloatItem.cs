﻿using UnityEngine;

/// <summary>
/// 漂浮物品接口
/// </summary>
public interface IFloatItem 
{
    Transform FloatTran { get; set; }

    /// <summary>
    /// 漂浮开始
    /// </summary>
    void OnFloatStart();
    /// <summary>
    /// 漂浮结束
    /// </summary>
    void OnFloatComplete();
}
