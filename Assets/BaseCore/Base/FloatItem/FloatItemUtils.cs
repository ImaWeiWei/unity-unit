﻿using UnityEngine;
using DG.Tweening;
/// <summary>
/// 漂浮工具类
/// </summary>
public class FloatItemUtils
{
    public static void FloatOneByOne(IFloatItem[] items, Vector3 start, Vector3 end, float duration)
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].FloatTran.position = start;
            items[i].FloatTran.DOMove(end, duration).OnStart(items[i].OnFloatStart).OnComplete(items[i].OnFloatComplete).SetDelay(i * 0.5f);
        }
    }
}
