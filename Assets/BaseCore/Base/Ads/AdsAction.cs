﻿using System;

namespace orangesGame
{
    /// <summary>
    /// 广告的回调基类
    /// </summary>
    public class AdsAction
    {
        /// <summary>
        /// 是否已经加载
        /// </summary>
        public bool IsLoade { get; protected set; }
        /// <summary>
        /// 回调事件
        /// </summary>
        public AdsEventListen AdsEvent { get; protected set; }
        /// <summary>
        /// 成功看完广告的回调
        /// </summary>
        public Action<bool> OnComplete { get; set; }

        protected float _reloadTime = 10;

        public AdsAction()
        {
            IsLoade = false;
        }

        public virtual void OnLoad(string id)
        {
            IsLoade = true;
        }

        public virtual void OnClick(string id)
        {

        }

        public virtual void OnClose(string id)
        {
            IsLoade = false;
        }

        public virtual void OnShow(string id)
        {

        }

        public virtual void OnReward(string id)
        {

        }

        public virtual void OnError(string id, string msg)
        {
            IsLoade = false;

        }
    }
}
