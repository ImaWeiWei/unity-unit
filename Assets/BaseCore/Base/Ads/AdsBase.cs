﻿
using System;
using UnityEngine;

namespace orangesGame
{
    /// <summary>
    /// 广告基类,所有广告执行都需要继承该接口，并实现接口
    /// </summary>
    public class AdsBase
    {
        protected bool _isDebug = true;

        public virtual void Init(AdsArgs args)
        {
            if (_isDebug)
            {
                Debug.Log("HippoSdk 初始化");
            }
        }
        public virtual void LoadBanner(string id)
        {
            if (_isDebug)
            {
                Debug.Log("LoadBanner-----------------------:" + id);
            }
        }
        public virtual void LoadInterstitial(string id)
        {
            if (_isDebug)
            {
                Debug.Log("LoadInterstitial-----------------------:" + id);
            }
        }
        public virtual void LoadVideo(string id)
        {
            if (_isDebug)
            {
                Debug.Log("LoadVideo-----------------------:" + id);
            }
        }

        public virtual void ShowBanner(string id)
        {
            if (_isDebug)
            {
                Debug.Log("ShowBanner---------------------------:" + id);
            }
        }
        public virtual void ShowInterstitial(string id)
        {
            if (_isDebug)
            {
                Debug.Log("ShowInterstitial---------------------------:" + id);
            }
        }
        public virtual void ShowVideo(string id)
        {
            if (_isDebug)
            {
                Debug.Log("ShowVideo---------------------------:" + id);
            }
        }
        public virtual void SetDebug(bool isDebug)
        {
            _isDebug = isDebug;
        }
    }


    public class AdsArgs
    {
        public string Appkey;
        public string Appsecret;
        public string Appchannel;
        public string UMKey;
        public string[] BannerIDs;
        public string[] InterstitialIDs;
        public string[] VideoIDs;
        public AdsEventListen BannerEvent = new AdsEventListen();
        public AdsEventListen InterstitialEvent = new AdsEventListen();
        public AdsEventListen VideoEvent = new AdsEventListen();
    }

    /// <summary>
    /// 广告回调事件
    /// </summary>
    public class AdsEventListen
    {
        public Action<string> OnLoad;
        public Action<string> OnStart;
        public Action<string> OnShow;
        public Action<string> OnClick;
        public Action<string> OnReward;
        public Action<string> OnClose;
        public Action<string, string> OnError;
    }
}
