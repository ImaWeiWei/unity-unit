﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

namespace orangesGame
{

    public class AdsData
    {
        public string appKey;
        public string appSecret;
        public string appChannel;
        public string umKey;
        public string[] bannerIDs;
        public string[] interstitialIDs;
        public string[] videoIDs;
    }


    /// <summary>
    /// 广告SDK管理类
    /// </summary>
    public class AdsManager
    {
        public bool isDebug = false;
        public bool isTest = false;

        static AdsManager()
        {
            Instance = (AdsManager)Activator.CreateInstance(typeof(AdsManager), true);

        }

        public static AdsManager Instance { get; private set; }


        private AdsBase _adsvertisement;
        private Dictionary<string, AdsAction> _adsAction;

        #region 初始化
        public void Init<T, K>(AdsData data) where T : AdsAction, new() where K : AdsBase, new()
        {
            if (_adsAction == null)
            {
                _adsAction = new Dictionary<string, AdsAction>();
            }

            AdsArgs adsArgs = new AdsArgs();
            adsArgs.Appkey = data.appKey;
            adsArgs.Appsecret = data.appSecret;
            adsArgs.Appchannel = data.appChannel;
            adsArgs.UMKey = data.umKey;

            InitBanner<T>(data.bannerIDs, ref adsArgs);
            InitInterstitial<T>(data.interstitialIDs, ref adsArgs);
            InitVideo<T>(data.videoIDs, ref adsArgs);

            CreateAdsvertisement<K>(adsArgs);
        }

        /// <summary>
        /// 初始化banner
        /// </summary>
        /// <param name="id"></param>
        void InitBanner<T>(string[] ids, ref AdsArgs args) where T : AdsAction, new()
        {
            if (ids == null || ids.Length == 0) { return; }
            args.BannerIDs = ids;
            args.BannerEvent = CreateAdsAction<T>(ids).AdsEvent;

        }
        /// <summary>
        /// 初始化插页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ids"></param>
        void InitInterstitial<T>(string[] ids, ref AdsArgs args) where T : AdsAction, new()
        {
            if (ids == null || ids.Length == 0) { return; }
            args.InterstitialIDs = ids;
            args.InterstitialEvent = CreateAdsAction<T>(ids).AdsEvent;
        }
        /// <summary>
        /// 初始化视频
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ids"></param>
        void InitVideo<T>(string[] ids, ref AdsArgs args) where T : AdsAction, new()
        {
            if (ids == null || ids.Length == 0) { return; }
            args.VideoIDs = ids;
            args.VideoEvent = CreateAdsAction<T>(ids).AdsEvent;
        }

        void CreateAdsvertisement<T>(AdsArgs args) where T : AdsBase, new()
        {
            _adsvertisement = new T();
            _adsvertisement.Init(args);
        }
        #endregion

        #region 加载与展示
        public void LoadBanner(string id)
        {
            if (!isTest)
            {
                _adsvertisement.LoadBanner(id);
            }
        }

        public void ShowBanner(string id)
        {
            if (!isTest)
            {
                _adsvertisement.ShowBanner(id);
            }
        }

        public void LoadInterstitial(string id)
        {
            if (!isTest)
            {
                _adsvertisement.LoadInterstitial(id);
            }
        }

        public void ShowInterstitial(string id, Action<bool> onComplete = null)
        {
            if (!isTest)
            {
                OnComplete(id, onComplete);
                _adsvertisement.ShowInterstitial(id);
            }
            else
            {
                onComplete?.Invoke(true);
            }
        }

        public void LoadVideo(string id)
        {
            if (!isTest)
            {
                _adsvertisement.LoadVideo(id);
            }
        }

        public void ShowVideo(string id, Action<bool> onComplete = null)
        {
            if (!isTest)
            {
                OnComplete(id, onComplete);
                _adsvertisement.ShowVideo(id);
            }
            else
            {
                onComplete?.Invoke(true);
            }
        }
        #endregion


        #region 事件回调
        /// <summary>
        /// 创建一个事件回调
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        private AdsAction CreateAdsAction<T>(string[] ids) where T : AdsAction, new()
        {
            T t = new T();
            for (int i = 0; i < ids.Length; i++)
            {
                if (!_adsAction.ContainsKey(ids[i]))
                {
                    _adsAction.Add(ids[i], t);
                }
                else
                {
                    Debug.LogError("CreateAdAction is contain:" + ids[i]);
                }
            }
            return t;
        }
        /// <summary>
        /// 当前参数id下的广告是否加载成功
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsLoad(string id)
        {

            if (isTest) { return true; }

            if (_adsAction.ContainsKey(id))
            {
                return _adsAction[id].IsLoade;
            }
            Debug.LogError("IsLoad is not contain:" + id);
            return false;
        }

        public void OnComplete(string id, Action<bool> callback)
        {
            if (_adsAction.ContainsKey(id))
            {
                _adsAction[id].OnComplete = null;
                _adsAction[id].OnComplete = callback;
            }
        }
        #endregion
    }
}
