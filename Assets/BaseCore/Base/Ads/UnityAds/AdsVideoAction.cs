﻿
namespace orangesGame
{

    public class AdsVideoAction : AdsAction
    {

        private bool _isReward = false;

        public AdsVideoAction()
        {
            AdsEvent = new AdsEventListen();
            AdsEvent.OnLoad = OnLoad;
            AdsEvent.OnClick = OnClick;
            AdsEvent.OnShow = OnShow;
            AdsEvent.OnClose = OnClose;
            AdsEvent.OnError = OnError;
            AdsEvent.OnReward = OnReward;
        }

        public override void OnLoad(string id)
        {
            base.OnLoad(id);
        }


        public override void OnClose(string id)
        {
            base.OnClose(id);
            if (_isReward && OnComplete != null)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() =>
                {
                    OnComplete(_isReward);
                    _isReward = false;
                });
            }
            AdsManager.Instance.LoadVideo(id);
        }

        public override void OnReward(string id)
        {
            base.OnReward(id);
            _isReward = true;
        }

        public override void OnError(string id, string msg)
        {
            base.OnError(id, msg);
            Timer.Register(_reloadTime, () =>
            {
                AdsManager.Instance.LoadVideo(id);
            });
        }
    }
}
