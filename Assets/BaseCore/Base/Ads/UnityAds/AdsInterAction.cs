﻿namespace orangesGame
{
    /// <summary>
    /// 插页广告回调
    /// </summary>
    public class AdInterAction : AdsAction
    {

        public AdInterAction()
        {
            AdsEvent = new AdsEventListen();
            AdsEvent.OnLoad = OnLoad;
            AdsEvent.OnClick = OnClick;
            AdsEvent.OnShow = OnShow;
            AdsEvent.OnClose = OnClose;
            AdsEvent.OnError = OnError;
        }

        public override void OnLoad(string id)
        {
            base.OnLoad(id);
        }

        public override void OnClose(string id)
        {
            base.OnClose(id);
            OnComplete?.Invoke(true);
            AdsManager.Instance.LoadInterstitial(id);
        }

        public override void OnError(string id, string msg)
        {
            base.OnError(id, msg);
            Timer.Register(_reloadTime, ()=> {
                AdsManager.Instance.LoadInterstitial(id);
            });
        }
    }
}
