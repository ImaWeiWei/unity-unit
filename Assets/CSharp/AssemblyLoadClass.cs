﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

/// <summary>
/// 利用反射获取指定命名空间下的所有类
/// </summary>
public class AssemblyLoadClass : MonoBehaviour
{
    void Start()
    {
        Assembly assembly = Assembly.GetExecutingAssembly();
        foreach (Type type in assembly.GetTypes())
        {
            if (type.Namespace == "A.B.C")
            {
                Debug.Log(type.ToString());
            }
        }
        
    }


}



