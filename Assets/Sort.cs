﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sort : MonoBehaviour
{

    private void Awake()
    {
        int[] arry = { 2, 3, 3, 2 };
        SortQuick(arry, 0, arry.Length - 1);
        for (int i = 0; i < arry.Length; i++)
        {
            Debug.Log(arry[i]);
        }
    }

    void SortQuick(int[] arry, int l, int r)
    {
        if (l >= r) { return; }
        int index = SortQuickUnit(arry, l, r);
        SortQuick(arry, l, index - 1);
        SortQuick(arry, index + 1, r);
    }

    int SortQuickUnit(int[] arry, int l, int r)
    {
        int key = arry[l];
        while (l < r)
        {
            while (l < r && arry[r] > key)
            {
                r--;
            }
            arry[l] = arry[r];
            while (l < r && arry[l] <= key)
            {
                l++;
            }
            arry[r] = arry[l];
        }
        arry[l] = key;
        return r;
    }
}
