﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnumMaskField))]
public class EnumMaskFieldInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        serializedObject.ApplyModifiedProperties();
    }
}
